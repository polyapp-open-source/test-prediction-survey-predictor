package main

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/polyapp-open-source/polyapp/actions"
	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
)

// Example TestPredictionSurveyHandler Test which makes sure 400 is returned for empty request bodies.
func TestTestPredictionSurveyHandler(t *testing.T) {
	r := httptest.NewRequest(http.MethodPost, "/", bytes.NewReader([]byte{}))
	w := httptest.NewRecorder()
	TestPredictionSurveyHandler(w, r)
	if w.Result().StatusCode != 400 {
		t.Error("expect StatusCode 400 bad request if body is empty")
	}
}

// Example DoTestPredictionSurvey Test which does not actually do anything.
func TestDoTestPredictionSurvey(t *testing.T) {
	var err error
	initialData := &common.Data{
		IndustryID: "Education and Health Services",
		DomainID: "Survey",
		SchemaID: "ODPZbcXuqQOLMlFYNzVnPvqur",
		FirestoreID: "TestDoTestPredictionSurvey",
	}
	initialData.Init(nil)
	err = allDB.Create(initialData)
	if err != nil {
		t.Fatal(err.Error())
	}
	httpsCallRequest := actions.HTTPSCallRequest{
		CurrentData:    map[string]interface{}{
			common.PolyappIndustryID: "Education and Health Services",
			common.PolyappDomainID: "Survey",
			common.PolyappSchemaID: "ODPZbcXuqQOLMlFYNzVnPvqur",
		},
		ReferencedData: map[string]map[string]interface{}{},
		Request:        &common.POSTRequest{
			DataID: "TestDoTestPredictionSurvey",
		},
		Response:       &common.POSTResponse{},
		BotStaticData:  map[string]string{},
	}
	httpsCallResponse := actions.HTTPSCallResponse{
		Request:     &common.POSTRequest{},
		Response:    &common.POSTResponse{},
		CreateDatas: []common.Data{},
		UpdateDatas: []common.Data{},
		DeleteDatas: []string{},
	}
	err = DoTestPredictionSurvey(&httpsCallRequest, &httpsCallResponse)
	if err != nil {
		t.Error(err.Error())
	}
	err = allDB.DeleteData("TestDoTestPredictionSurvey")
	if err != nil {
		t.Fatal(err.Error())
	}
	if httpsCallResponse.Response.DataUpdates != nil && len(httpsCallResponse.Response.DataUpdates) > 0 {
		for k, v := range httpsCallResponse.Response.DataUpdates {
			if k == "" {
				t.Errorf("httpsCallResponse.Response.DataUpdates[%v] key was empty - this will be invalid on the client", k)
			}
			for fieldKey := range v {
				if fieldKey == "" {
					t.Errorf("httpsCallResponse.Response.DataUpdates[%v][%v] fieldKey was invalid because it was empty", k, fieldKey)
				}
				if !strings.HasPrefix(fieldKey, "polyapp") {
					ind, dom, sch, field := common.SplitField(fieldKey)
					if ind == "" || dom == "" || sch == "" || field == "" {
						t.Errorf("httpsCallResponse.Response.DataUpdates[%v][%v] fieldKey was invalid because ind / dom / sch / field was empty - this will be invalid on the client", k, fieldKey)
					}
				}
			}
		}
	}
}
