package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"gitlab.com/polyapp-open-source/polyapp/actions"
	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8081"
	}
	http.HandleFunc("/", TestPredictionSurveyHandler)

	// TLSPath is used for local development on SSL.
	TLSPath, TLSPathExists := os.LookupEnv("TLSPATH")
	if !TLSPathExists {
		log.Fatal(http.ListenAndServe(":"+port, nil))
	} else {
		cert := TLSPath + string(os.PathSeparator) + "server.crt"
		key := TLSPath + string(os.PathSeparator) + "server.key"
		log.Fatal(http.ListenAndServeTLS(":"+port, cert, key, nil))
	}
}

type TestPredictionSurvey struct {
	Email                              string    `suffix:"Email"`
	LoadUserIDs                        []string  `suffix:"Load User IDs"`
	LoadTimestamps                     []float64 `suffix:"Load Timestamps"`
	Doyouusecreaminyourcoffee          string    `suffix:"Do you use cream in your coffee?"`
	FavoriteNumber                     float64   `suffix:"Favorite Number"`
	ProbabilityYouAreLactoseIntolerant float64   `suffix:"Probability You Are Lactose Intolerant"`
	DoneUserIDs                        []string  `suffix:"Done User IDs"`
	Done                               bool      `suffix:"Done"`
	DoneTimestamps                     []float64 `suffix:"Done Timestamps"`
	FavoriteColor                      string    `suffix:"Favorite Color"`
	Doyoulikemilk                      string    `suffix:"Do you like milk?"`
	Doyoudrinkcoffeedaily              string    `suffix:"Do you drink coffee daily?"`
}

// TestPredictionSurveyHandler translates a request into a format usable by func TestPredictionSurvey
// It should be triggered via a POST request since a body is required, but any HTTP verb is accepted.
func TestPredictionSurveyHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("ioutil.ReadAll: %v", err.Error()), 400)
		return
	}
	var HTTPSCallRequest actions.HTTPSCallRequest
	HTTPSCallResponse := actions.HTTPSCallResponse{
		Request:     &common.POSTRequest{},
		Response:    &common.POSTResponse{},
		CreateDatas: []common.Data{},
		UpdateDatas: []common.Data{},
		DeleteDatas: []string{},
	}
	fmt.Println(string(body))
	err = json.Unmarshal(body, &HTTPSCallRequest)
	if err != nil {
		http.Error(w, fmt.Sprintf("json.Unmarshal: %v", err.Error()), 400)
		return
	}
	err = DoTestPredictionSurvey(&HTTPSCallRequest, &HTTPSCallResponse)
	if err != nil {
		http.Error(w, fmt.Sprintf("DoTestPredictionSurvey: %v", err.Error()), 500)
		return
	}
	responseBytes, err := json.Marshal(HTTPSCallResponse)
	if err != nil {
		http.Error(w, fmt.Sprintf("json.Marshal: %v", err.Error()), 500)
		return
	}
	fmt.Println(string(responseBytes))
	_, err = w.Write(responseBytes)
	if err != nil {
		http.Error(w, fmt.Sprintf("w.Write: %v", err.Error()), 500)
		return
	}
}

// DoTestPredictionSurvey Hello and welcome to this test survey :)
//
// In this survey we tried to motivate the person filling out the survey by showing them a prediction of the survey's outcome as they fill the survey out.
//
// To do this, a machine learning model was used to predict the value of a survey field. This field is shown in the upper right hand corner.
func DoTestPredictionSurvey(request *actions.HTTPSCallRequest, response *actions.HTTPSCallResponse) error {
	var err error
	data := common.Data{}
	err = data.Init(request.CurrentData)
	if err != nil {
		return fmt.Errorf("data.Init: %w", err)
	}
	data.FirestoreID = request.Request.DataID
	TestPredictionSurvey := TestPredictionSurvey{}
	err = common.DataIntoStructure(&data, &TestPredictionSurvey)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}

	TestPredictionSurvey.ProbabilityYouAreLactoseIntolerant = 0.5
	if TestPredictionSurvey.Doyoulikemilk == "Yes" {
		TestPredictionSurvey.ProbabilityYouAreLactoseIntolerant = 0.1
	} else if TestPredictionSurvey.Doyoulikemilk == "No" {
		TestPredictionSurvey.ProbabilityYouAreLactoseIntolerant = 0.9
	}

	dataUpdatesData := common.Data{
		FirestoreID: data.FirestoreID,
		IndustryID:  data.IndustryID,
		DomainID:    data.DomainID,
		SchemaID:    data.SchemaID,
		F: map[string]*float64{
			common.AddFieldPrefix(data.IndustryID, data.DomainID, data.SchemaID, "Probability You Are Lactose Intolerant"): common.Float64(TestPredictionSurvey.ProbabilityYouAreLactoseIntolerant),
		},
	}

	response.Response.DataUpdates, err = common.DataToPOSTData(&dataUpdatesData, *request.Request)
	if err != nil {
		return fmt.Errorf("common.DataToPOSTData: %w", err)
	}

	// Save any updates which happened to Data into the database
	err = allDB.UpdateData(&data)
	if err != nil {
		return fmt.Errorf("allDB.updateData: %w", err)
	}

	return nil
}
